for room in $(ls); do
	if [ -a ../../linux-adm/linux-adm/spec/$room/data/macs ]; then
		list=$(cat ../../linux-adm/linux-adm/spec/$room/data/macs | cut -b -2)
		for computer in $list; do
			mkdir "$room""$computer"
		done
	fi
done
